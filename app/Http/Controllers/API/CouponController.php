<?php

namespace App\Http\Controllers\API;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Coupon::with('promo')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $coupon = Coupon::create(['promo_id' => $request->promo_id, 'user_id' => $request->user_id, 'code' => Hash::make($request->promo_id . $request->user_id)]);
            return response()->json(['success' => true, 'coupon' => $coupon], 200);
        } catch (Exception $e){
            return response()->json(['success' => false, 'message' => 'Ha ocurrido un error, prueba más tarde.'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        //
    }

    /**
     * Returns a Coupon for specified promo based on user_id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCouponPromo(Request $request)
    {
        try{
            $coupon = Coupon::where('promo_id', $request->promo_id)->where('user_id', $request->user_id)->first();
            return response()->json(['success' => true, 'coupon' => $coupon], 200);
        } catch (Exception $e){
            return response()->json(['success' => false, 'message' => 'Ha ocurrido un error, prueba más tarde.'], 200);
        }
    }

    /**
     * Validates a coupon
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validateCoupon(Request $request)
    {
        try{
            $coupon = Coupon::find($request->id);
            $coupon->redeemed = 1;
            $coupon->save();
            return response()->json(['success' => true, 'coupon' => $coupon], 200);
        } catch (Exception $e){
            return response()->json(['success' => false, 'message' => 'Ha ocurrido un error, prueba más tarde.'], 200);
        }
    }
}
