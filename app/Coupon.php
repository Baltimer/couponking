<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'promo_id', 'user_id', 'redeemed', 'code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    // Relationships

    /**
     * Get the relationship between Coupon and Promo
     */
    public function promo() {
        return $this->hasOne('App\Promo', 'id', 'promo_id');
    }
}
