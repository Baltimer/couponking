<?php

use Faker\Generator as Faker;

$factory->define(App\Promo::class, function (Faker $faker) {
    return [
        'image' => 'promo-' . rand(1,3) . '.jpg',
        'name' => $faker->name,
        'description' => $faker->sentence(7, true)
    ];
});
