# Instrucciones para arrancar el proyecto
- Clonar el proyecto en una carpeta local
- Acceder a la carpeta y ejecutar los siguientes comandos:  
    `composer install`  
    `npm install`  
    `copy .env.example .env`  
    `php artisan key:generate`  
- Crear una base de datos MySql en local con el nombre __coupons__
- Ejecutar el comando:  
    `php artisan migrate --seed`  
    \* Si fuese necesario, cambiar la configuración del archivo __.env__ para conectar a la base de datos en local que tengas definida.  
- Iniciar el servidor y acceder a la url que nos facilite:  
    `php artisan serve`  
    
# Aclaraciones
- Se han subido los archivos __app.js__ y __app.css__ ya compilados para que no sea necesaria la compilación de dichos archivos a la hora de ejecutar la aplicación. Si fuese necesario, ejecutar el comando `npm build` para compilar de nuevo los archivos.
- La prueba se ha realizado en un total de 3 horas.
- Se ha utilizado xampp para la elaboración de la prueba.
- Se han utilizado los frameworks [Vuejs](https://vuejs.org/) y [Laravel](https://laravel.com/), así como una plantilla de admin ([AdminLTE3](https://adminlte.io/themes/dev/AdminLTE/index.html)).